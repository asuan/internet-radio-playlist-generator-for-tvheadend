# Internet radio playlist generator for TVHeadend

Třeba to někomu ulehčí práci

vytvořil jsem sadu scriptů, pro vygenerování playlistu z konfigurační tabulky streamů

Ověřená funkčnost se zdroji mime type:  
* MP3  
* FLAC  
* WAV  
* AAC  

Nefunguje se zdroji mime type:  
* WMA  
* MID  

## Dependence:
* GNU/Linux  
* bash  
* node.js  
**v případě použití ffmpeg v "listen_radio.sh" (o tom níže), možno následující vynechat:**  
* gstreamer1.0  
* gstreamer1.0-plugins-bad  
* curl  

## Instalace a použití:
* Stáhnout soubory z repozitáře (zip nebo "git clone"), v případě zip souboru rozbalit do složky  
* Editovat soubor "**config.js**", v parametru "**playlistPath**" změnit cestu kam chcete playlist uložit, "radioList" lze upravit dle preferencí, nechtěná rádia odebrat či přidat jiné  
* Pokud vám nevyhovuje nebo nemáte nainstalovaný GStreamer, ale FFMpeg, lze editovat soubor "**listen_radio.sh**" a změnit příkaz commanline  
* Spustit příkaz "bash generate_internet_radio_playlist.sh"  
* Nyní máte vygenerovaný playlist v místě dle cesty v configu  

## Import do TVH:  
* Přihlaste se do webové administrace TVHeadendu  
* V "Konfigurace -> DVB Vstupy -> Sítě" vytvořte novou síť  
* Typ sítě zvolte "IPTV Automatic Network" a otevře se vám další okno  
* Povoleno: Ano  
* Název: dle vašich preferencí, já si zvolil "Internet Radio"  
* Vytvořit buket: já mám **ne**, protože jsem ještě nepřišel na to jak to používat, ale pokud víte co a jak, není problém povolit  
* Max. počet vstupních streamů: není důvod nijak omezovat takže 0  
* Max. šířka pásma: 0, tato volba nemá vliv na bitrate streamů  
* URL: cesta k playlistu ve formátu "**file://** + **playlistPath** z configu" (např.: "file:///home/hts/iradio.m3u8")  
* Počáteční číslo programu: 0 (níže povolíme ignorování tohoto číslování)  
* Accept zero value for TSID: povolit  
* Název sítě poskytovatele: lze nechat prázdné  
* ID síťě (limit pro vyhledávání): 0  
* Ignorovat čísla programů od poskytovatele: povolit  
* SAT>IP číslo zdroje: 0  
* Znaková sada: není třeba volit žádnou znakovou sadu  
* Use A/V library: ne  
* Skenovat po vytvoření: ne, scan budeme spouštět ručně  
* Priorita: 1  
* Priorita streamu: 1  
* Maximální čas odezvy (sekund): 15 (default)
* Základní URL pro ikony: nechat prázdné  
* Znaková sada pro obsah: není třeba volit žádnou znakovou sadu  
* Perioda pro nový pokus (v minutách): 7200 (default)  
* Skip startup scan: ano, scan budeme spouštět ručně  
* Vyhledávání muxů při nečinnosti: ne, jednou jsem to zkusil povolit a procesor jel na 90%, má význam u reálných tunerů
* zbytek lze nechat na defaultních hodnotách  
* Tlačítko "**Vytvoř**"  
* Vybereme nově vytvořenou síť a v horní liště klikneme na tlačítko "**Vynutit scan**"  
* Přejdeme na záložku "Konfigurace -> DVB Vstupy -> Muxy"  
* Nyní máme naimportovány všechny položky z playlistu, co rádio to MUX  
* Pokud výsledek scanu není "OK", změňte stav scanu z "NEČINNÝ" na "NEDOKONČENÝ" a uložte  
* Ve chvíli, kdy výsledek scanu je "OK" se pro každý rádiomux vytvoří služba  
* V záložce "Služby" si namapujte rádio služby, tím se vytvoří kanály a od této chvíle je lze přehrávat (např. v Kodi)  
* Kanály je vhodné oštítkovat, určitě jin dejte štítek "Radio channels" (já jsem si jěště přidal např. jazyk "CZ" a že se jedná o internetové rádio, pojmenoval jsem jako "DAB-I")  
* To je vše, nyní můžete poslouchat oblíbená rádia prostřednictvím TVH

## Soubory:

**generate_internet_radio_playlist.sh**  
spouštěcí script generátoru, obsahuje cestu k logu (default: /var/log/tvheadend/Internet_radio_playlist_generator.log)  

**config.js**  
obsahuje cestu k m3u8 playlistu a tabulku streamů  
```javascript
module.exports = {
	playlistPath: '/home/hts/iradio.m3u8',
	radioList: [
		//{ name: 'Název služby v TVH',	uri: 'http://URI/STREAMU', },
		{ name: 'Rádio Impuls',		uri: 'http://icecast1.play.cz/impuls128.mp3', },
		{ name: 'ČRo Radiožurnál',	uri: 'http://icecast7.play.cz/cro1-128.mp3', },
		{ name: 'ČRo Dvojka',		uri: 'http://icecast6.play.cz/cro2-128.mp3', },
		{ name: 'ČRo Vltava',		uri: 'http://icecast5.play.cz/cro3-128.mp3', },
		{ name: 'ČRo Plus',		uri: 'http://icecast1.play.cz/croplus128.mp3', },
		{ name: 'ČRo Wave',		uri: 'http://icecast6.play.cz/crowave-128.mp3', },
		{ name: 'ČRo D-Dur',		uri: 'http://icecast5.play.cz/croddur-128.mp3', },
		{ name: 'ČRo Jazz',		uri: 'http://icecast1.play.cz/crojazz128.mp3', },
		{ name: 'ČRo Junior',		uri: 'http://icecast5.play.cz/crojuniormaxi128.mp3', },
		{ name: 'ČRo Retro',		uri: 'http://icecast7.play.cz/croretro128.mp3', },
		// a mnoho dalších
	],
}
```

**main.js**  
vlastní script pro generování, není potřeba nic měnit, veškeré variabilní nastavení jsou v config.js  

**listen_radio.sh**  
tento script se volá z TVH při požadavku přehrávání rádia, URI se předává parametrem  
zde se nachází vlastní příkaz, v mém případě je použit GStreamer, ale je možno použít ffmpeg  
využil jsem služeb GStreameru, jelikož se mi nepovedla rozchodit pipe pomocí ffmpeg  
