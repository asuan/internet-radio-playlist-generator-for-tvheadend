#!/bin/bash

URI=$1

gst-launch-1.0 -q curlhttpsrc location=$URI ! decodebin ! audioconvert ! audioresample ! avenc_aac ! mpegtsmux ! fdsink
