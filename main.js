const { writeFile } = require('fs');
const config = require('./config.js');

function logger(message) {
	var date = new Date();
	console.log(date.toLocaleString('cs-CZ') + ' :  ' + JSON.stringify(message));
}

function writePlaylist(path, content) {
	return new Promise(function (resolve, reject) {
		writeFile(path, content + "\n", (err) => {
			if (err) {
				reject('File ' + path + ' is not writable.');
				return;
			}
			resolve(1);
			return;
		});
	});
}

function getCommand(uri) {
	const scriptName = 'listen_radio.sh';

	return '"' + __dirname + '/' + scriptName + '" "' + uri + '"';
}

function main() {
	var playlistContent = [];

	playlistContent.push('#EXTM3U');
	
	config.radioList.map((radio, index) => {
		const url = getCommand(radio.uri)

		logger(radio.name);
		playlistContent.push('#EXTINF:-1,' + radio.name);

		logger(url);
		playlistContent.push('pipe://' + url );
	});

	writePlaylist(config.playlistPath, playlistContent.join("\n"))
	.then(function(radios) {
		logger('Playlist ' + config.playlistPath + ' created.');
	})
	.catch(function(message) {
		logger(JSON.stringify(message));
	});
}

main();

