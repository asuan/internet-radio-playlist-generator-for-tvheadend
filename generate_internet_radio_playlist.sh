#!/bin/bash

LOGROOT=/var/log/tvheadend
LOGPATH=$LOGROOT/Internet_radio_playlist_generator.log
DIR=$(dirname "$(readlink -f "$0")")

cd "$DIR"

if ! [ -d $LOGROOT ]; then
	mkdir $LOGROOT
	chmod 777 $LOGROOT
fi

if ! [ -f $LOGPATH ]; then
	echo "" > "$LOGPATH"
fi

node main.js $@ >> "$LOGPATH"
